package com.gtl.android.timeonscreen

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*


@SuppressLint("SimpleDateFormat")
fun doWork(activity: Activity) {
    activity.runOnUiThread {
        try {
            val txtCurrentDate = activity.findViewById<View>(R.id.dateTV) as TextView
            val txtCurrentTime = activity.findViewById<View>(R.id.timeTV) as TextView
            val dt = Date()
            val dateFormat = SimpleDateFormat("dd MMM yyyy")
            val timeFormat = SimpleDateFormat("hh:mm:ss")
            txtCurrentDate.text = dateFormat.format(dt)
            txtCurrentTime.text = timeFormat.format(dt)
            Log.e("currentTime", dt.toString())
        } catch (e: Exception) {
        }
    }
}

var activity = Any()

class MainActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = ""
        setContentView(R.layout.activity_main)
        activity = this
        val myThread: Thread?
        val runnable: Runnable = CountDownRunner()
        myThread = Thread(runnable)
        myThread.start()

        Log.e("activityStarted", "|" + Date().toString() + "|")
    }


    internal class CountDownRunner : Runnable {
        override fun run() {
            while (!Thread.currentThread().isInterrupted) {
                try {
                    doWork(activity as MainActivity)
                    Thread.sleep(1000)
                } catch (e: InterruptedException) {
                    Thread.currentThread().interrupt()
                } catch (e: Exception) {
                }
            }
        }
    }

    fun onLeaveClick(view: View) {
        finish()
    }
}